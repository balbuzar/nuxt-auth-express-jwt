const express = require("express");
const cors = require("cors");
const jwt = require("jsonwebtoken");
const bearerToken = require("express-bearer-token");
const bodyParser = require("body-parser");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJWT = require("passport-jwt").ExtractJwt;

const app = express();
const http = require("http").Server(app);

app.set("etag", false);
app.set("trust proxy", 1);
// app.use(cookieParser("randomsecret"));
app.use(bodyParser.json({ limit: "1mb" }));
app.use(bodyParser.urlencoded({ limit: "1mb", extended: true }));

const awesomeUser = {
  id: 42,
  username: "test"
};

const JwtStrategySecret = "anotherrandomsecret";
passport.use(
  new LocalStrategy(function(username, password, done) {
    if (username === "test" && password === "test") {
      return done(null, awesomeUser);
    }
    return done(null, false);
  })
);

passport.use(
  new JwtStrategy(
    {
      secretOrKey: JwtStrategySecret,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
    },
    (jwtPayload, done) => {
      console.log("JWT Payload", jwtPayload);
      if (jwtPayload.id == 42) {
        console.log("JWT user found");
        return done(null, awesomeUser);
      }
      return done(null, false);
    }
  )
);
passport.serializeUser((user, cb) => {
  cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
  cb(null, awesomeUser);
});

app.use((req, res, next) => {
  console.log(req.method, req.originalUrl);
  next();
});

app.use(
  cors({
    origin: true,
    credentials: true
  })
);

app.post(
  "/api/auth/login",
  // cors({
  //     // credentials: true,
  // }),
  passport.authenticate("local", { session: false }),
  (req, res) => {
    // console.log('headers', res.getHeaders())
    // console.log('req.body', req.body)
    console.log("req.user", req.user);

    const token = jwt.sign(
      {
        id: req.user.id
      },
      JwtStrategySecret,
      { expiresIn: 60 * 60 * 24 }
    );

    console.log("> sending token", req.body, {
      success: true,
      token
    });
    res.json({
      success: true,
      token
    });
  }
);

app.get(
  "/api/auth/user",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json({
      success: true,
      user: awesomeUser
    });
  }
);

const port = 3001;
http.listen(port, "127.0.0.1", function() {
  console.log(`Server started on port ${port}`);
});
