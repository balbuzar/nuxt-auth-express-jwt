export default function({ $axios, store, isDev }) {
  $axios.setBaseURL("http://localhost:3001");

  $axios.onRequest(config => {
    console.log("AXIOS Config", JSON.stringify(config, null, 2));
  });
  $axios.onError(error => {
    console.log("axios error", error);
  });
}
