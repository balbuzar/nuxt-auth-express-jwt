# nuxt-auth-express-jwt

## Setup

```bash
# install dependencies
$ npm install

# [Term 1] serve api server at localhost:3001
$ node api-server.js

# [Term 2] serve with hot reload at localhost:3000
$ npm run dev

```

## Nuxt setup

```
Choose programming language JavaScript
Choose the package manager Npm
Choose UI framework Bulma
Choose custom server framework None
Choose Nuxt.js modules Axios
Choose linting tools None
Choose test framework None
Choose rendering mode Universal (SSR)
Choose development tools None
```

## How to reproduce the issue

### Initialization

- `node api-server.js` on a terminal 
- `npm run dev` on a second terminal, 
- load http://localhost:3000 on your browser
- click on **Login** link
- use credentials _test_ // _test_ 

### First Submit 

#### API Server Log

```
OPTIONS /api/auth/login
```

#### Browser Console Log

```
XHR failed loading: POST "http://localhost:3001/api/auth/login"
```

#### Browser Network Log

```
/api/auth/login` (canceled)
```

###### General

```
Request URL: http://localhost:3001/api/auth/login
Referrer Policy: no-referrer-when-downgrade
```

###### Request Headers

```/!\ Provisional headers are shown
Accept: application/json, text/plain, */*
Content-Type: application/json;charset=UTF-8
DNT: 1
Referer: http://localhost:3000/login?
Sec-Fetch-Dest: empty
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36
```

###### Request Payload

```
{username: "test", password: "test"}
```

### Second Submit

#### API Server Log

```OPTIONS /api/auth/login
POST /api/auth/login
req.user { id: 42, username: 'test' }
sending token { username: 'test', password: 'test' } {
success: true,
token: 'xxxxxxxxxxxxxxxxxxxxxx.xxxxxxxx'
}
```

#### Browser Console Log

```
XHR failed loading: POST "http://localhost:3001/api/auth/login"
```

=> why ? api server answers and browser network log seems to be ok

#### Browser Network Log

```
/api/auth/login` (canceled)
```

###### General

```
Request URL: http://localhost:3001/api/auth/login
Referrer Policy: no-referrer-when-downgrade
```

###### Response Headers

```
Access-Control-Allow-Credentials: true
Access-Control-Allow-Origin: http://localhost:3000
Connection: keep-alive
Content-Length: 166
Content-Type: application/json; charset=utf-8
Date: Sat, 04 Apr 2020 09:08:11 GMT
Vary: Origin
X-Powered-By: Express
```

###### Request Headers

```
Accept: application/json, text/plain, */*
Accept-Encoding: gzip, deflate, br
Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6
Connection: keep-alive
Content-Length: 37
Content-Type: application/json;charset=UTF-8
Cookie: auth.strategy=local; auth._refresh_token.local=false; auth._token.local=false
DNT: 1
Host: localhost:3001
Origin: http://localhost:3000
Referer: http://localhost:3000/login?
Sec-Fetch-Dest: empty
Sec-Fetch-Mode: cors
Sec-Fetch-Site: same-site
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36
```

###### Request Payload

```
{username: "test", password: "test"}
```

### Third, fourth... Submits

Same as first submit, click until the login request succeed

### Fifth Submit

#### API Server Log

```
OPTIONS /api/auth/login
POST /api/auth/login
req.user { id: 42, username: 'test' }
sending token { username: 'test', password: 'test' } {
success: true,
token: 'xxxxxxxxxxxxxxxxxxxxxx.xxxxxxxx'
}
```

#### Browser Console Log

```
XHR finished loading: POST "http://localhost:3001/api/auth/login"
```

#### Browser Network Log

```
/api/auth/login` 200
```

###### General

```
Request URL: http://localhost:3001/api/auth/login
Request Method: POST
Status Code: 200 OK
Remote Address: 127.0.0.1:3001
Referrer Policy: no-referrer-when-downgrade
```

###### Response Headers

```
Access-Control-Allow-Credentials: true
Access-Control-Allow-Origin: http://localhost:3000
Connection: keep-alive
Content-Length: 166
Content-Type: application/json; charset=utf-8
Date: Sat, 04 Apr 2020 09:11:35 GMT
Vary: Origin
X-Powered-By: Express
```

###### Request Headers

```
Accept: application/json, text/plain, */*
Accept-Encoding: gzip, deflate, br
Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6
Connection: keep-alive
Content-Length: 37
Content-Type: application/json;charset=UTF-8
Cookie: auth.strategy=local; auth._refresh_token.local=false; auth._token.local=false
DNT: 1
Host: localhost:3001
Origin: http://localhost:3000
Referer: http://localhost:3000/login?
Sec-Fetch-Dest: empty
Sec-Fetch-Mode: cors
Sec-Fetch-Site: same-site
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36
```

###### Request Payload

```
{username: "test", password: "test"}
```

## Some questions

On the second (or more) submit, when api server and browser network log correctly answer, why request is marqued as "canceled" ?

On postman, if I use`/api/auth/login` and then `/api/auth/me` (with the received token as Bearer token) it perfectly works.

Is the problem on $auth, on $axios or in my implementation ?